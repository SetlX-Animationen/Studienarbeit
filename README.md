## CI-Builds
<img alt="build status" src="https://gitlab.com/SetlX-Animationen/Studienarbeit/badges/master/build.svg"/>

<div>[<img alt="Download latest Build-Result" src="http://i.imgur.com/Dt6AxwZ.png" width="70px">](https://gitlab.com/SetlX-Animationen/Studienarbeit/builds/artifacts/master/file/main.pdf?job=build_latex)


---


## How to Use
1. place the entries for your bibliography into `./resources/references.bib`
2. place the `.tex`-files containing your content into `./content` and define the structure of your content inside `./content/structure.tex`
3. fill your acronyms as needed into `./content/acronyms.tex`
4. save your image files into `./resources`
    - you can then use them easily by just referencing `\includegraphics{asdf}` if you saved your file at `./resources/asdf.png`



## Included Custom Elements for Ease of Use
**Note:** `<asdf>` inside the general code denotes a placeholder


#### striped Tables
```Latex
\begin{stripedacenttable}
    {<caption>}
    {\label{tab:<label>}}
    {<formating>}
    {<Headings-Content>}
    <row definitions>
\end{stripedacenttable}

\begin{stripedtable}
    {<caption>}
    {\label{tab:<label>}}
    {<coloring>}
    {<formating>}
    {<Headings-Content>}
    <row definitions>
\end{stripedtable}
```

- *<label>* needs to be enclosed inside `\label` to keep the auto-completion functionality of your editor working correctly
- formating should have the form `x^x^x^...` where `x` specifies the alignment for the column
    + possible aligments: `l`: left-aligned , `c`: centered , `r`: right-aligned

> *Example (with captions):*
> ```Latex
> \begin{stripedacenttable}
>    {A plain but nice looking table}
>    {\label{tab:ex1}}
>    {c^l^l}
>    {Quarter & asdf & foobar}
>    prev. Year & 42 & 17 \\
>    Q1 & -3 & -7 \\
>    Q2 & +7 & -1 \\
>    Q3 & -4 & +12 \\
>    Q4 & +2 & +2 \\
>\end{stripedacenttable}
>
>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Ut purus elit, vestibulum ut, placerat ac, adipiscing vitae, felis.
>
>\begin{stripedtable}
>    {A colorful, nice looking table}
>    {\label{tab:ex1}}
>    {Green}
>    {c^l^l}
>    {Quarter & asdf & foobar}
>    prev. Year & 42 & 17 \\
>    Q1 & -3 & -7 \\
>    Q2 & +7 & -1 \\
>    Q3 & -4 & +12 \\
>    Q4 & +2 & +2 \\
>\end{stripedtable}
>```
> <img alt="Tables" src="http://i.imgur.com/8FzhRYr.png" width="65%">


#### Code Listings
```Latex
\begin{lstlisting}[
    caption={<description of your program>},
    label={lst:<label>},
    captionpos=b,
    language=<language-name>
]
<your code>
\end{lstlisting}
```

> *Example:*
> ```Latex
>\begin{lstlisting}[
>    caption={The Classic, realized in Python},
>    label={lst:python1},
>    captionpos=b,
>    language=Python
>]
> # classic
>
> hi = "Hello Wolrd"
>print(hi)
>\end{lstlisting}
>```
> <img alt="Code Lisitng" src="http://i.imgur.com/8zXqzOZ.png" width="70%">


#### Boxed Text
```Latex
Nam dui ligula, fringilla a, euismod sodales, sollicitudin vel, wisi. Morbi auctor lorem non justo. Nam lacus libero, pretium at, lobortis vitae, ultricies et, tellus.

\begin{notebox}{<border-color>}{<icon>}
<your text>
\end{notebox}

\begin{infobox}
<your text>
\end{infobox}

\begin{warnbox}
<your text>
\end{warnbox}
```

> *Example:*
> ```Latex
>Nam dui ligula, fringilla a, euismod sodales, sollicitudin vel, wisi. Morbi auctor lorem non justo. Nam lacus libero, pretium at, lobortis vitae, ultricies et, tellus.
>
>\begin{notebox}{Green}{\faStar{}}
>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Ut purus elit, vesti- bulum ut, placerat ac, adipiscing vitae, felis. Curabitur dictum gravida mauris. Nam arcu libero, nonummy eget, consectetuer id, vulputate a, magna. Donec vehicula augue eu neque.
>\end{notebox}
>
>Nam dui ligula, fringilla a, euismod sodales, sollicitudin vel, wisi. Morbi auctor lorem non justo. Nam lacus libero, pretium at, lobortis vitae, ultricies et, tellus.
>
>\begin{infobox}
>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Mauris ut leo. Cras viverra metus rhoncus sem. Nulla et lectus vestibulum urna fringilla ultrices. Phasellus eu tellus sit amet tortor gravida placerat.
>\end{infobox}
>
>Nam dui ligula, fringilla a, euismod sodales, sollicitudin vel, wisi. Morbi auctor lorem non justo. Nam lacus libero, pretium at, lobortis vitae, ultricies et, tellus.
>
>\begin{warnbox}
>Integer sapien est, iaculis in, pretium quis, viverra ac, nunc. Praesent eget sem vel leo ultrices bibendum. Aenean faucibus. Morbi dolor nulla, malesuada eu, pulvinar at, mollis ac, nulla. Curabitur auctor semper nulla. Donec varius orci eget risus.
>\end{warnbox}
>
>Nam dui ligula, fringilla a, euismod sodales, sollicitudin vel, wisi. Morbi auctor lorem non justo. Nam lacus libero, pretium at, lobortis vitae, ultricies et, tellus.
>```
> <img alt="Code Lisitng" src="http://i.imgur.com/7inW1M0.png" width="70%">


#### Fancy Symbols
The FontAwesome Package is bundeled into this repository, so you can use [all the nice symbols.](http://mirrors.ctan.org/fonts/fontawesome/doc/fontawesome.pdf).

> *Example:*
> ````Latex
> \faFileTextO{} \faStar{}\faStar{}\faStar{}\faStar{}\faStar{} $=$ \faGraduationCap{}
> ````
> <img alt="Code Lisitng" src="http://i.imgur.com/wZAVaom.png" width="14%">

The MarVoSym-Package is also loaded to provide [additional symbols](http://texdoc.net/texmf-dist/doc/fonts/marvosym/marvodoc.pdf).

> *Example:*
> ````Latex
> \Estatically{} \Forward{} \Printer{} \ \ \MVRightArrow{} \ \ \EyesDollar\EyesDollar\EyesDollar
> ````
> <img alt="Code Lisitng" src="http://i.imgur.com/lZ64aQA.png" width="14%">


#### Citations in the Footnotes
````Latex
\footnotecite{<source-reference>}
````


#### Mark Incomplete Things You Need To Do
````Latex
\incompletemarker{<note>}
````

> <img alt="Code Lisitng" src="http://i.imgur.com/eSQSoao.png" width="40%">


#### Prevent Pagebreaks absolutely, definitively
````Latex
\begin{absolutelynopagebreak}
    <content>
\end{absolutelynopagebreak}
````
